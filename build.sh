#! /bin/bash

randstr=$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 8 | head -n 1)
export randstr

bash build_pkg.sh
source cleanup.sh