#! /bin/bash

mkdir -p /usr/local/share/ca-certificates
cp /tmp/tuntox-pkg/ssl/certs/localhost.crt /usr/local/share/ca-certificates/localhost.crt
update-ca-certificates

#Building tuntox
cd /tmp && git clone --branch 0.0.9 --depth 1 https://github.com/gjedeer/tuntox.git
cd /tmp/tuntox && make

#Build debian package
mkdir -p /tmp/deb-pkg/
cp -r /tmp/tuntox-pkg/debian/ /tmp/deb-pkg/
cp /tmp/tuntox/tuntox /tmp/deb-pkg
cd /tmp/deb-pkg && debuild -b -us -uc
cp /tmp/*.deb /tmp/tuntox-pkg