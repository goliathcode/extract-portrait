FROM debian:10

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
   && apt-get install -y python3-pip python3-numpy

RUN pip3 install opencv-python==3.4.15.55

WORKDIR /exportrait

# Clean up to reduce image size
RUN apt-get clean && \
    rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/*