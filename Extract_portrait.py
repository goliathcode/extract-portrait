import cv2
import numpy as np
import sys
import os

i=1 #for saving each face
# Get the current working  
# directory (CWD)  
cwd = os.getcwd()  
      
# Print the current working directory (CWD)  
print("Current working directory:", cwd)

# Get the directory of current script
script = os.path.realpath(__file__)
print("Current script path:", script)
csd = os.path.dirname(script)
print("Current script directory:", csd)

faceCascade = cv2.CascadeClassifier(os.path.join(csd, 'haarcascade_frontalface_default.xml'))
left_eyeCascade = cv2.CascadeClassifier(os.path.join(csd, 'haarcascade_lefteye_2splits.xml'))
right_eyeCascade = cv2.CascadeClassifier(os.path.join(csd, 'haarcascade_righteye_2splits.xml'))
input_file = "" # Absolute path of image file
output_dir = "" # Absolute path of output folder

# Display File name 
#print("Script name ", sys.argv[0])
# Display the first argument
#print(f"first arg {sys.argv[1]}")
if len(sys.argv) >= 2:
    input_file = sys.argv[1]

if len(sys.argv) >= 3:
    output_dir = sys.argv[2]

if input_file == "":
    input_file = input('Enter Full Image Path: ')

if output_dir == "":
    output_dir = os.path.dirname(input_file)

img = cv2.imread(input_file)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

faces = faceCascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(40, 40))

for (x,y,w,h) in faces:
    #Draw rectangle for face
    #img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    roi_gray = gray[y:y+h, x:x+w]
    roi_color = img[y:y+h, x:x+w]
    right_eyes = right_eyeCascade.detectMultiScale(roi_gray)

    #Draw rectangle for right eye
    #for (rex,rey,rew,reh) in right_eyes:
    #    cv2.rectangle(roi_color,(rex,rey),(rex+rew,rey+reh),(0,255,0),2)
    left_eyes = left_eyeCascade.detectMultiScale(roi_gray)

    #Draw rectangle for left eye
    #for (lex,ley,lew,leh) in left_eyes:
    #    cv2.rectangle(roi_color,(lex,ley),(lex+lew,ley+leh),(0,255,0),2)

    fullpath_output = os.path.join(output_dir, "face{0}.png".format(i))
    #saves each face in independent images:
    if len(right_eyes) != 0 or len(left_eyes) != 0:
        cropped=img[y-int(h/1.5):y+int(1.7*h) , x-int(w/2):x+int(1.5*w)]
        cv2.imwrite(fullpath_output, cropped)
    i+=1

print("Found {0} faces!".format(len(faces)))


