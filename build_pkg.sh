#! /bin/bash

containerid=exportrait-build-$randstr
imageid=exportrait-build-$(id -u)

(set -xe; docker build -t $imageid .)

set -xe

chmod a+x setup_build.sh

#docker run --rm -d --name dat-gateway-$randstr dat-gateway
#dat_gateway_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dat-gateway-$randstr)

mkdir -p "$(dirname $(readlink -f "$0"))/ssl/private"
mkdir -p "$(dirname $(readlink -f "$0"))/ssl/certs"

#docker run --rm -it \
#    --name ssl-gen-$randstr \
#    -v "$(dirname $(readlink -f "$0"))/ssl/private:/etc/ssl/private" \
#    -v "$(dirname $(readlink -f "$0"))/ssl/certs:/etc/ssl/certs" \
#    dat-dedns-proxy openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/localhost.key -out /etc/ssl/certs/localhost.crt -config /ssl_conf/localhost.conf

#docker run --rm -d \
#    --name dat-dedns-proxy-$randstr \
#    -v "$(dirname $(readlink -f "$0"))/ssl:/etc/ssl" \
#    dat-dedns-proxy

#dat_dedns_proxy_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dat-dedns-proxy-$randstr)

docker run -it --rm \
    --name $containerid \
    -v "$(dirname $(readlink -f "$0")):/exportrait" \
    $imageid

    #--add-host dat.local:$dat_gateway_ip \
    #--add-host ssl.icu-project.org:$dat_dedns_proxy_ip \
