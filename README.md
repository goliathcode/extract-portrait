# Extract Portrait

Detects and crops the head and shoulder part of a still image.  Extract each detected portrait to a new image.

## Dependencies

- python 3.6+
- opencv-python
- numpy

## Usage

### Extract
Run `Extract_portrait.py` to extract individual portraits from an image.

### Display
Run `Haar-cascade-Detection.py` to generate a new image showing all detected portraits.

